/*
    1. Declare 3 variables without initialization called username,password and role.
*/

/*
    
    2. Create a login function which is able to prompt the user to provide their username, password and role.
        -use prompt() and update the username,password and role global variable with the prompt() returned values.
        -add an if statement to check if the the username is an empty string or null or if the password is an empty string or null or if the role is an empty string or null.
            -if it is, show an alert to inform the user that their input should not be empty.
        -add an else statement. Inside the else statement add a switch to check the user's role add 3 cases and a default:
                -if the user's role is admin, show an alert with the following message:
                    "Welcome back to the class portal, admin!"
                -if the user's role is teacher, show an alert with the following message:
                    "Thank you for logging in, teacher!"
                -if the user's role is a rookie, show an alert with the following message:
                    "Welcome to the class portal, student!"
                -if the user's role does not fall under any of the cases, as a default, show a message:
                    "Role out of range."
*/


/*
    3. Create a function which is able to receive 4 numbers as arguments calculate its average and log a message for  the user about their letter equivalent in the console.
        -add parameters appropriate to describe the arguments.
        -create a new function scoped variable called avg.
        -calculate the average of the 4 number inputs and store it in the variable avg.
        -add an if statement to check if the value of avg is less than or equal to 74.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show avg>. The letter equivalent is F"
        -add an else if statement to check if the value of avg is greater than or equal to 75 and if avg is less than or equal to 79.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show avg>. The letter equivalent is D"
        -add an else if statement to check if the value of avg is greater than or equal to 80 and if avg is less than or equal to 84.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show avg>. The letter equivalent is C"
        -add an else if statement to check if the value of avg is greater than or equal to 85 and if avg is less than or equal to 89.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show avg>. The letter equivalent is B"
        -add an else if statement to check if the value of avg is greater than or equal to 90 and if avg is less than or equal to 95.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show avg>. The letter equivalent is A"
        -add an else if statement to check if the value of avg is greater than 96.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show avg>. The letter equivalent is A+"

        Invoke and add a number as argument using the browser console.

        Stretch goal:
            -Add an if statement to check the role of the user.
                -if the role of the user is currently "teacher" or "admin" or undefined or null, show an alert:
                "<role>! You are not allowed to access this feature!".
                -else, continue to evaluate the letter equivalent of the student's average.

*/

let username;
let password;
let role;



function main(){

    function Input_Checker(){
        if(username === ""){
            alert("username/password cannot be empty");
            login();


       }else if(password === ""){
            alert("username/password cannot be empty");
            login();

       } else {

            switch(role){

                case "admin":
                alert("Welcome back to the class portal, admin!");
                break;

                case "teacher":
                alert("Thank you for logging in, teacher!");
                break;

                case "rookie":
            
                alert("Welcome to the class portal, student!");
                break;

                default:
                alert("Role out of range.");
                break;
            }
       }
        
    }

    function login(){
       username = prompt("Enter username:").trim();
       password = prompt("Enter password:").trim();
       role = prompt("Enter role").toLowerCase();
       Input_Checker()  
    };

    function avg_Checker(num1, num2, num3, num4){
        avg = (num1 + num2 + num3 + num4) / 4;

        if (avg <= 74) {
            console.log("Hello, student, your average is " + avg + " The letter equivalent is F")
        } else if (avg >= 75, avg <=79, avg < 80){
            console.log("Hello, student, your average is " + avg + " The letter equivalent is D")
        } else if (avg >= 80, avg <=84, avg < 85){
            console.log("Hello, student, your average is " + avg + " The letter equivalent is C")
        } else if (avg >= 85, avg <=89, avg < 90){
            console.log("Hello, student, your average is " + avg + " The letter equivalent is B")
        } else if (avg >= 90, avg <=95, avg < 96){
            console.log("Hello, student, your average is " + avg + " The letter equivalent is A")
        } else if (avg >= 96){
            console.log("Hello, student, your average is " + avg + " The letter equivalent is A+")
        }

    };

    login();
    avg_Checker(70,74,73,72);
    avg_Checker(79,78,78,80);
    avg_Checker(80,80,79,84);
    avg_Checker(85,85,86,89);
    avg_Checker(90,90,90,93);
    avg_Checker(96,96,97,100);
}

main();

